import React, { useState } from "react";

const AddItemForm = (props) => {
  const intitialFormState = { id: `${Date.now()}`, name: "", description: "" };

  const [item, setItem] = useState(intitialFormState);

  const handleInputChange = (event) => {
    const { name, value } = event.target;

    setItem({ ...item, [name]: value });
  };

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        if (!item.name || !item.description) return;
        props.addItem(item);
        setItem(intitialFormState);
      }}
    >
      <label>Item</label>
      <input
        type="text"
        name="name"
        value={item.name}
        onChange={handleInputChange}
      />
      <label>Description</label>
      <input
        type="text"
        name="description"
        value={item.description}
        onChange={handleInputChange}
      />
      <button>Add New Item</button>
    </form>
  );
};

export default AddItemForm;
