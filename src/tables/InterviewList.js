import React from "react";

const InerviewList = (props) => {
  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Start Date</th>
          <th>End Date</th>
        </tr>
      </thead>
      <tbody>
        {props.items.length > 0 ? (
          props.items.map((item) => (
            <tr key={item.id}>
              <td>{item.name}</td>
              <td>{item.description}</td>
              <td>{item.startDate}</td>
              <td>{item.endDate}</td>
              {/* <td>
                <button
                  className="button muted-button btn-edit"
                  onClick={() => {
                    props.editRow(item);
                  }}
                >
                  Edit
                </button>
                <button
                  onClick={() => props.deleteItem(item.id)}
                  className="button muted-button btn-remove"
                >
                  Delete
                </button>
              </td> */}
            </tr>
          ))
        ) : (
          <tr>
            <td colSpan={3}>No Items Found</td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export default InerviewList;
