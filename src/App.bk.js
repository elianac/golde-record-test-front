import "./App.css";
import React from "react";
import axios from "axios";

const apiURL = "https://localhost:7065/weatherForecast";

function App() {
  const [interviews, setInterviews] = React.useState([]);

  React.useEffect(() => {
    axios.get(apiURL).then((res) => {
      setInterviews(res.data);
    });
    console.log(interviews);
  }, []);

  return (
    <div className="App">
      {interviews.map((e) => (
        <div key={e.id}>
          <h2>{e.name}</h2>
          <h2>{e.description}</h2>
          <h3>{e.startDate}</h3>
          <h3>{e.endDate}</h3>
        </div>
      ))}
    </div>
  );
}

export default App;
