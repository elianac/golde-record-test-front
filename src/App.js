import React, { useState, Fragment, useEffect } from "react";
import InterviewList from "./tables/InterviewList";
import AddItemForm from "./components/AddItemForm";
import EditItemForm from "./components/EditItemForm";
import axios from "axios";
import "./App.css";

// const apiURL = "http://localhost:5291/api/Interview";
const apiURL = "http://192.81.214.135:5000/api/interview";

const App = () => {
  const initialFormState = { id: null, name: "", description: "" };

  const [editing, setEditing] = useState(false);
  const [currentItem, setCurrentItem] = useState(initialFormState);

  const editRow = (item) => {
    setEditing(true);

    setCurrentItem({
      id: item.id,
      name: item.name,
      description: item.description,
      // startDate: item.startDate,
      // endDate: item.endDate,
    });
  };

  // Edit Item
  const updateItem = (id, updateItem) => {
    setEditing(false);

    axios.put(`${apiURL}/${id}`, updateItem).then((response) => {
      setInterviews(response.data);
    });
    console.log("*** UPDATE Operation ***", updateItem);
  };

  // Add Item
  const addItem = (item) => {
    axios.post(apiURL, item).then((response) => {
      setInterviews(response.data);
    });
    console.log("*** POST Operation ***", item);
  };

  // Delete Item
  const deleteItem = (id) => {
    setEditing(false);
    axios.delete(`${apiURL}/${id}`).then(() => {
      setInterviews(null);
    });
    console.log("*** DELETE Operation ***", id);
  };

  // Load interviews
  const [interviews, setInterviews] = useState([]);

  useEffect(() => {
    axios.get(apiURL).then((res) => {
      setInterviews(res.data);
    });
    console.log(interviews);
  }, []);

  return (
    <div className="container">
      <h1>Interviews</h1>
      <div className="flex-row">
        {/* <div className="flex-large">
          {editing ? (
            <Fragment>
              <h2>Edit Item</h2>
              <EditItemForm
                setEditing={setEditing}
                editing={editing}
                currentItem={currentItem}
                updateItem={updateItem}
              />
            </Fragment>
          ) : (
            <Fragment>
              <h2>Add Interview</h2>
              <AddItemForm addItem={addItem} />
            </Fragment>
          )}
        </div> */}
        <div className="flex-large">
          <h2>View List</h2>
          <InterviewList
            items={interviews}
            deleteItem={deleteItem}
            editRow={editRow}
          />
        </div>
      </div>
    </div>
  );
};

export default App;
